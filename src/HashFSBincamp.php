<?php

namespace AzureSpring\Bincamp;

use League\Uri\Uri;
use League\Uri\UriModifier;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use League\Uri\Components\HierarchicalPath as Path;

class HashFSBincamp implements BincampInterface
{
    private $fs;

    private $root;

    private $base_url;


    public function __construct( Filesystem $fs, $root, $base_url = '' )
    {
        $this->fs   = $fs;
        $this->root = $root;
        $this->base_url = Uri::createFromString( $base_url );
    }

    public function find( $name, array $cd = array() )
    {
        return implode(
            DIRECTORY_SEPARATOR,
            array(
                $this->root,
                implode(
                    DIRECTORY_SEPARATOR,
                    $this->findRelative( $name, $cd )
                )
            )
        );
    }

    public function save( \SplFileInfo $file, $ext = null )
    {
        $original = $file->getRealPath() ?: $file->getPathname();
        $sha1 = sha1_file( $original );
        if ( !$ext )
            $ext = $file->getExtension();

        $name = implode( '.', array_filter(array( $sha1, $ext )) );
        $path = $this->find( $name );

        try {
            $this->fs->mkdir( dirname($path) );
        }
        catch ( IOExceptionInterface $e ) {
            // ignore
        }
        $this->fs->copy( $original, $path );

        return $name;
    }

    public function findURI( $name, array $cd = array() )
    {
        $uri = $this->base_url;
        foreach ( $this->findRelative($name, $cd) as $seg )
            $uri = UriModifier::appendSegment( $uri, $seg );

        return $uri;
    }

    public function polarize( $loc )
    {
        $cd = Path::createFromString( $loc )->segments();

        $filename = array_pop( $cd );
        if ( substr($filename, 2, 2) != array_pop($cd) ||
             substr($filename, 0, 2) != array_pop($cd) )
            return false;

        return array( $filename, $cd );
    }

    private function findRelative( $name, array $cd = array() )
    {
        return array_merge(
            $cd,
            array( substr($name, 0, 2),
                   substr($name, 2, 2),
                   $name )
        );
    }
}
