<?php

namespace AzureSpring\Bincamp\ImagickFilter;

class CropFilter implements ImagickFilterInterface
{
    private $width;

    private $height;


    public function __construct( $width, $height )
    {
        $this->width  = $width;
        $this->height = $height;
    }

    public function apply( \Imagick $img )
    {
        foreach ( $img as $im ) {
            $im->cropThumbnailImage( $this->width, $this->height );
        }
    }
}
