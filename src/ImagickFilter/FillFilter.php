<?php

namespace AzureSpring\Bincamp\ImagickFilter;

class FillFilter implements ImagickFilterInterface
{
    private $width;

    private $height;

    private $bestfit;


    public function __construct( $width, $height, $bestfit = false )
    {
        $this->width    = $width;
        $this->height   = $height;
        $this->bestfit  = $bestfit;
    }

    public function apply( \Imagick $img )
    {
        foreach ( $img as $im ) {
            $im->thumbnailImage( $this->width, $this->height, $this->bestfit );
        }
    }
}
