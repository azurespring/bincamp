<?php

namespace AzureSpring\Bincamp\ImagickFilter;

interface ImagickFilterInterface
{
    public function apply( \Imagick $img );
}
