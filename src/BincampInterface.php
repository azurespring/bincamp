<?php

namespace AzureSpring\Bincamp;

interface BincampInterface
{
    public function find( $name, array $cd = array() );
    public function save( \SplFileInfo $file, $ext = null );

    public function findURI( $name, array $cd = array() );
    public function polarize( $loc );
}
