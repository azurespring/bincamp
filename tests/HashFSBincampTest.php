<?php

namespace AzureSpring\Bincamp\Tests;

use AzureSpring\Bincamp\HashFSBincamp;
use Symfony\Component\Filesystem\Filesystem;

class HashFSBincampTest extends \PHPUnit\Framework\TestCase
{
    private $fs;

    private $tmp;

    private $tmpdir;

    private $bincamp;

    public function setUp(): void
    {
        $this->fs = new Filesystem();
        $this->tmp = tempnam( '', '' );
        file_put_contents( $this->tmp, <<<__FILE__
hello, world.
__FILE__
        );

        $this->tmpdir = tempnam( '', '' );
        $this->fs->remove( $this->tmpdir );
        $this->fs->mkdir( $this->tmpdir );

        $this->bincamp = new HashFSBincamp( $this->fs, $this->tmpdir );
    }

    public function tearDown(): void
    {
        $this->fs->remove( $this->tmp );
        $this->fs->remove( $this->tmpdir );
    }

    public function testInstantiate()
    {
        $this->assertInstanceOf( HashFSBincamp::class, $this->bincamp );
    }

    public function testFind()
    {
        $this->assertEquals(
            implode(DIRECTORY_SEPARATOR, [ $this->tmpdir, 'a1/a6/a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf' ]),
            $this->bincamp->find( 'a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf' )
        );
        $this->assertEquals(
            implode(DIRECTORY_SEPARATOR, [ $this->tmpdir, '.o/a1/a6/a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf' ]),
            $this->bincamp->find( 'a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf', ['.o'] )
        );
    }

    public function testSave()
    {
        $file = $this->getMockBuilder( \SplFileInfo::class )
            ->disableOriginalConstructor()
            ->getMock()
        ;
        $file
            ->method( 'getRealPath' )
            ->willReturn( $this->tmp )
        ;
        $file
            ->method( 'getExtension' )
            ->willReturn( '' )
        ;

        $this->assertEquals(
            'a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf',
            $this->bincamp->save( $file )
        );
        $filename = implode(
            DIRECTORY_SEPARATOR,
            [ $this->tmpdir, 'a1/a6/a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf' ]
        );
        $this->assertFileExists( $filename );
        $this->assertEquals( 'a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf', sha1_file($filename) );
    }

    public function testFindURI()
    {
        $this->assertEquals(
            '/a1/a6/a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf',
            $this->bincamp->findURI('a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf')
        );
        $this->assertEquals(
            '/.o/a1/a6/a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf',
            $this->bincamp->findURI('a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf', [ '.o' ])
        );

        $bincamp = new HashFSBincamp( $this->fs, $this->tmpdir, 'http://example.com/fs' );
        $this->assertEquals(
            'http://example.com/fs/a1/a6/a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf',
            $bincamp->findURI('a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf')
        );
        $this->assertEquals(
            'http://example.com/fs/.o/a1/a6/a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf',
            $bincamp->findURI('a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf', [ '.o' ])
        );
    }

    public function testPolarize()
    {
        $this->assertEquals(
            [ 'a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf', ['.o'] ],
            $this->bincamp->polarize('.o/a1/a6/a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf')
        );
    }
}
