<?php

namespace AzureSpring\Bincamp\Tests\ImagickFilter;

use AzureSpring\Bincamp\ImagickFilter\FillFilter;

class FillFilterTest extends \PHPUnit\Framework\TestCase
{
    private $filter;

    public function setUp(): void
    {
        $this->filter = new FillFilter( 400, 300 );
    }

    public function testInstantiate()
    {
        $this->assertInstanceOf( FillFilter::class, $this->filter );
    }

    public function testApply()
    {
        $imagick = $this
            ->getMockBuilder( \Imagick::class )
            ->getMock()
        ;
        $imagick
            ->method( 'valid' )
            ->willReturnOnConsecutiveCalls( true, false )
        ;
        $imagick
            ->method( 'current' )
            ->willReturn( $imagick )
        ;
        $imagick
            ->expects( $this->once() )
            ->method( 'thumbnailImage' )
            ->with( 400, 300, false )
        ;

        $this->filter->apply( $imagick );
    }
}
