<?php

namespace AzureSpring\Bincamp\Tests\ImagickFilter;

use AzureSpring\Bincamp\ImagickFilter\CropFilter;

class CropFilterTest extends \PHPUnit\Framework\TestCase
{
    private $filter;

    public function setUp(): void
    {
        $this->filter = new CropFilter( 400, 300 );
    }

    public function testInstantiate()
    {
        $this->assertInstanceOf( CropFilter::class, $this->filter );
    }

    public function testApply()
    {
        $imagick = $this
            ->getMockBuilder( \Imagick::class )
            ->getMock()
        ;
        $imagick
            ->method( 'valid' )
            ->willReturnOnConsecutiveCalls( true, false )
        ;
        $imagick
            ->method( 'current' )
            ->willReturn( $imagick )
        ;
        $imagick
            ->expects( $this->once() )
            ->method( 'cropThumbnailImage' )
            ->with( 400, 300 )
        ;

        $this->filter->apply( $imagick );
    }
}
